import compression from 'compression';
import logger from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import { env } from './environment';

export default (app) => {
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('X-powered-by', 'Apogee Live Casino');
    next();
  });
  app.use(compression());
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(bodyParser.urlencoded({ extended : true }));

  if (env === 'development') {
    app.use(logger('dev'));
  }
};
