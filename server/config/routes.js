export default (app) => {
  app.use('/services', require('../services'));

  // Default route.
  app.route('*')
    .get((req, res) => {
      res.send('<h1>ApogeeLive API Server</h1><h2>Version 0.0.0</h2>');
    });
};
