import { port } from '../../../package.json';

const all = {
  env     : process.env.NODE_ENV || 'development',
  port    : process.env.PORT || port,
  db_host : '190.241.12.163',
  db_port : 11433,
};

export default Object.assign(all, require(`./${all.env}.js`));
