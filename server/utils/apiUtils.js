export function notFoundResponse (msg, res) {
  return res.status(404).json({ error : 'NotFound', message : msg });
}
