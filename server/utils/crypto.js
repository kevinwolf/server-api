import crypto from 'crypto';

const ALGORITHM = 'aes-256-ctr';
const PASSWORD = '4p0ge3l!v3';

export function encrypt (text) {
  const cipher = crypto.createCipher(ALGORITHM, PASSWORD);
  let crypted = cipher.update(text.toString(), 'utf-8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

export function decrypt (text) {
  const decipher = crypto.createDecipher(ALGORITHM, PASSWORD);
  let dec = decipher.update(text, 'hex', 'utf-8');
  dec += decipher.final('utf8');
  return dec;
}
