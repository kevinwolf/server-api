import express from 'express';

const router = new express.Router();
router.post('/auth', require('./auth'));

export default router;
