import sql from 'mssql';
import config from '../../config/environment';
import { encrypt } from '../../utils/crypto';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'LoginFailed', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('Login', sql.VarChar(100), req.body.Login);
      request.input('Password', sql.VarChar(100), encrypt(req.body.Password));
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('Authenticated', sql.Bit);
      request.output('AdminId', sql.Int);
      request.output('LoginStatus', sql.TinyInt);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spLogin_Admin]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'LoginFailed', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            const token = encrypt(`${encrypt('ADMIN')}|${encrypt(req.body.Login)}|${req.body.Password}`);
            res.cookie('APOGEE_TOKEN',
              token,
              { maxAge   : 15 * 60 * 1000,
                secure   : false,
                httpOnly : true,
              });
            res.status(200).json({
              token : token,
            });
          }else {
            res.status(401).json({
              error   : 'LoginFailed',
              message : request.parameters.output_STATUS.value,
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'LoginFailed', message : 'The Login information is incorrect' });
  }
}
