import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to take spots', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      const params = {
        Casino_GameId : '1',
        UID           : req.body.tableId,
        DealerId      : req.body.dealerId,
        // NOTE: this should not be called FreeSpots, it should be called Taken Spots
        FreeSpots     : req.body.takenSpots,
      };

      const tableSpotsXML = json2xml({TableGameS : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
      console.log(tableSpotsXML);
      request.input('TableSpots_Param', sql.VarChar(1000), tableSpotsXML);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spUpdate_TableSpots]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to take spots', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            res.status(200).json({
              message : request.parameters.output_STATUS.value,
            });
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : `Unable to take spots ${request.parameters.output_STATUS.value}`,
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotTakingSpots', message : 'Unable to take spots, try again' });
  }
}
