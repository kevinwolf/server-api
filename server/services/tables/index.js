import express from 'express';

const router = new express.Router();
router.get('/list/:licenseeId', require('./list'));
router.post('/edit', require('./edit'));
router.post('/login', require('./login'));
router.post('/takeSpots', require('./takeSpots'));

export default router;
