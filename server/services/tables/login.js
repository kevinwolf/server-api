import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to login to table 1', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      const params = {
        Casino_GameId : '1',
        UID           : req.body.uid,
        CardId        : req.body.cardId,
      };
      const tableLoginXml = json2xml({TableDealer : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
      request.input('TableDealer_Param', sql.VarChar(1000), tableLoginXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spDealer_Login]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to login to table 2', message : errSpExecution.message });
        } else {
          console.log(tableLoginXml);
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            res.status(200).json({message : request.parameters.output_STATUS.value});
          }else {
            console.log(request.parameters);
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to login to table 3',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotLoginTable', message : 'Unable to login to table, try again' });
  }
}
