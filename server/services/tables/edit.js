import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
import listTables from './list';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to edit table', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      const params = {
        BJK_TableId     : req.body.tableId,
        UID             : req.body.uid,
        NumSpots        : req.body.numSpots,
        TableMin        : req.body.tableMin,
        TableMax        : req.body.tableMax,
        MaxSplits       : req.body.maxSplits,
        SplitAces       : req.body.splitAces ? '1' : '0',
        Double          : req.body.double ? '1' : '0',
        AllowSideBets   : req.body.allowSideBets ? '1' : '0',
        Entity_SideBets : req.body.entitySideBets,
        Active          : req.body.active ? '1' : '0',
      };
      const websiteXml = json2xml({TableGame : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
      request.input('BJKTable_Param', sql.VarChar(1000), websiteXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spUpdate_BJKTables]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to edit table', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            listTables(req, res);
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to edit Tables',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingTable', message : 'Unable to create Table, try again' });
  }
}
