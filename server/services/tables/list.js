import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
import _ from 'lodash';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const formatResult = function formatResult (result) {
  let formattedResult = {};
  try {
    if (result.TableGames) {
      const blackjackData = result.TableGames.BlackJack[0];
      const baccaratData = result.TableGames.Baccarat[0];
      const americanRouletteData = result.TableGames.Roulette_A[0];
      const europeanRouletteData = result.TableGames.Roulette_E[0];

      formattedResult = {
        blackjack : {
          id     : blackjackData.$.Casino_Game_Id,
          active : blackjackData.$.Active,
          tables : _.map(blackjackData.Tables[0].Table, function formatBlackjack (table) {
            return {
              tableId        : table.BJK_TableId[0],
              uid            : table.UID[0],
              numSpots       : table.NumSpots[0],
              tableMin       : table.TableMin[0],
              tableMax       : table.TableMax[0],
              maxSplits      : table.MaxSplits[0],
              splitAces      : table.SplitAces[0],
              double         : table.Double[0],
              allowSideBets  : table.Allow_SideBets[0],
              entitySideBets : table.Entity_SideBets[0],
              active         : table.Active[0],
            };
          }),
        },
        baccarat : {
          id     : baccaratData.$.Casino_Game_Id,
          active : baccaratData.$.Active,
          tables : _.map(baccaratData.Tables[0].Table, function formatBaccarat (table) {
            return {
              tableId        : table.Bacrt_TableId[0],
              uid            : table.UID[0],
              tableMin       : table.TableMin[0],
              tableMax       : table.TableMax[0],
              allowSideBets  : table.Allow_SideBets[0],
              entitySideBets : table.Entity_SideBets[0],
              active         : table.Active[0],
            };
          }),
        },
        americanRoulette : {
          id     : americanRouletteData.$.Casino_Game_Id,
          active : americanRouletteData.$.Active,
          tables : [],
        },
        europeanRoulette : {
          id     : europeanRouletteData.$.Casino_Game_Id,
          active : europeanRouletteData.$.Active,
          tables : [],
        },
      };
    }
  } catch (e) {
    console.log(e);
  }
  return formattedResult;
};
export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Oh oh! Unable to get Tables', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('LicenseeId', sql.Int, null);
      request.input('IS_DEBUG', sql.Bit, 0);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGetTables]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to get Tables :(', message : errSpExecution.message });
        } else {
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              res.status(401).json({ error : 'Incorrect Parse', message : error.message });
            }
            if (request.parameters.output_IS_SUCCESSFUL.value) {
              res.status(200).json(formatResult(result));
            }else {
              res.status(401).json({
                error   : 'Output not successful',
                message : request.parameters.output_STATUS,
              });
            }
          });
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NoListOfTables', message : 'Unable to get tables, try again' });
  }
}
