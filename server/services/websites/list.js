import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
import _ from 'lodash';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const formatResult = function formatResult (result) {
  let formattedResult = {};
  try {
    if (result.WebSite.Detail[0].Table[0]) {
      formattedResult = _.map(result.WebSite.Detail[0].Table, function licenseeMap (websiteObj) {
        return {
          id         : websiteObj.WebSiteId[0],
          licenseeId : websiteObj.LicenseeId[0],
          header     : websiteObj.Host_Header[0],
        };
      });
    }
  } catch (e) {
    console.log(e);
  }
  return formattedResult;
};
export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Oh oh! Unable to get Websites', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('LicenseeId', sql.Int, req.params.licenseeId || req.body.licensee);
      request.input('IS_DEBUG', sql.Bit, 0);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGetWebSite]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to get Licensees :(', message : errSpExecution.message });
        } else {
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              res.status(401).json({ error : 'Incorrect Parse', message : error.message });
            }
            if (request.parameters.output_IS_SUCCESSFUL.value) {
              res.status(200).json(formatResult(result));
            }else {
              res.status(401).json({
                error   : 'Output not successful',
                message : request.parameters.output_STATUS,
              });
            }
          });
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NoListOfDealers', message : 'Unable to get dealers, try again' });
  }
}
