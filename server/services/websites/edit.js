import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
import listWebsites from './list';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to edit website', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      const params = {Licensee_Id : req.body.licensee, Host_Header : req.body.header, WebSiteId : req.body.id};
      const websiteXml = json2xml({WebSites : {Detail : params, attr : params}}, { attributes_key : 'attr' });
      request.input('WebSite_Param', sql.VarChar(1000), websiteXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spUpdate_WebSites]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to edit website', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            listWebsites(req, res);
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to edit Website',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingWebsite', message : 'Unable to create Website, try again' });
  }
}
