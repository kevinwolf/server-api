import express from 'express';

const router = new express.Router();
router.get('/list/:licenseeId', require('./list'));
router.post('/create', require('./create'));
router.post('/edit', require('./edit'));

export default router;
