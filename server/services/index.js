import { Router } from 'express';
const router = new Router();


router.use('/admins', require('./admins'));

router.use('/customers', require('./customers'));

router.use('/operators', require('./operators'));
router.use('/websites', require('./websites'));
router.use('/agents', require('./agents'));
router.use('/tables', require('./tables'));
router.use('/games', require('./games'));
router.use('/blackjack', require('./blackjack'));

router.use('/create-dealer', require('./dealers'));
router.use('/get-dealers', require('./dealers'));
router.use('/update-dealer', require('./dealers'));
router.use('/deactive-dealer', require('./dealers'));

router.use('/create-table', require('./blackjack'));
router.use('/add-gamelog', require('./blackjack'));
router.use('/settings', require('./blackjack'));
router.use('/cust-actions', require('./blackjack'));
router.use('/add-sidebet', require('./blackjack'));

router.use('/add-game', require('./casino-games'));
router.use('/update-game', require('./casino-games'));

router.use('/add-lobby-game', require('./lobby'));
router.use('/add-lobby', require('./lobby'));

export default router;
