import express from 'express';

const router = new express.Router();
router.post('/add-lobby-game', require('./add-lobby-game'));
router.post('/add-lobby', require('./add-lobby'));

export default router;
