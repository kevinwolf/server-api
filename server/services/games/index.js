import express from 'express';

const router = new express.Router();
router.post('/create', require('./create'));
router.post('/update', require('./update'));

export default router;
