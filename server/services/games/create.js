import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
import async from 'async';
import createPlayerLog from './createPlayerLog';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to create game', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      const params = {
        CasinoGameId : '1',
        UID          : req.body.tableUID,
        DealerId     : req.body.dealerId,
        Card1        : req.body.card1,
        Card2        : req.body.card2,
        Extra_Card   : req.body.extraCard,
        Total        : req.body.total,
        NumAces      : req.body.numAces,
        NumCards     : req.body.numCards,
        Bet_Amount   : req.body.betAmount,
        'Bet-Result' : req.body.betResult,
      };
      const gameXml = json2xml({BJKGame : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
      request.input('BJKGame_Param', sql.Xml, gameXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spBJK_CreateGame]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to create game', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            const positionLogs = [];
            async.each(req.body.playerLogs, function playerLogFn (playerLog, callback) {
              playerLog.gameId = dataset[0][0].GameId;
              createPlayerLog(playerLog).then((logId) => {
                positionLogs.push({
                  gameLogId : logId,
                  position  : playerLog.position,
                });
                callback();
              }).catch((err) => {
                callback(err);
              });
            }, (err) => {
              if (err) {
                res.status(401).json({
                  error   : 'Oops! Try again',
                  message : err.message,
                });
              } else {
                res.status(200).json({
                  gameId       : dataset[0][0].GameId,
                  positionLogs : positionLogs,
                });
              }
            });
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Game Create not successful',
            });
          }
        }
      });
    });
  } catch (err) {
    res.status(401).json({ error : 'NotCreatingTable', message : 'Unable to create game, try again' });
  }
}
