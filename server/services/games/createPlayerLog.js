import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (playerLog) {
  return new Promise((resolve, reject) => {
    try {
      const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
        const request = connection.request();
        if (errSpConnection) {
          reject(errSpConnection.message);
        }
        /* eslint-disable new-cap */
        const params = {
          CasinoGameId : '1',
          GameId       : playerLog.gameId,
          CustomerId   : playerLog.id,
          PosNum       : playerLog.position,
          BetAmount    : playerLog.betAmount,
          ResultAmt    : playerLog.result,
          InsAmount    : playerLog.insuranceAmount,
          InsResult    : playerLog.insuranceResult,
          SideBetId    : playerLog.sideBetId,
          NumSplits    : playerLog.splits,
          Card1        : playerLog.card1,
          Card2        : playerLog.card2,
          ExtraCard    : playerLog.extraCard,
          Total        : playerLog.total,
          NumAces      : playerLog.numAces,
          NumCards     : playerLog.numCards,
          Double       : playerLog.double,
          Error        : playerLog.error,
          ExecMs       : playerLog.execMS,
        };
        const gameLogXml = json2xml({BJK_GameLog : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
        request.input('BJK_GameLog_Param', sql.Xml, gameLogXml);
        request.input('param_IS_DEBUG', sql.Bit, null);
        request.output('output_IS_SUCCESSFUL', sql.Bit);
        request.output('output_STATUS', sql.VarChar(500));
        /* eslint-enable new-cap */
        request.execute('[dbo].[spBJK_AddGameLog]', function spExecution (errSpExecution, dataset) {
          connection.close();
          if (errSpExecution) {
            reject(errSpExecution.message);
          } else {
            if (request.parameters.output_IS_SUCCESSFUL.value) {
              resolve(dataset[0][0][''][0]);
            }else {
              reject(request.parameters.output_STATUS.value);
            }
          }
        });
      });
    } catch (err) {
      reject(err.message);
    }
  });
}
