import sql from 'mssql';
import config from '../../config/environment';
import {parseString} from 'xml2js';
import _ from 'lodash';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};
export default function () {
  return new Promise((resolve, reject) => {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        reject({ error : 'LoginFailed', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('param_CUSTOMER_ID', sql.Int, 17);
      request.input('param_CUSTOMER_TOKEN', sql.VarChar(250), '0');
      request.input('IS_DEBUG', sql.Bit, 0);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGetCustomerLobby]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          reject({
            error   : 'errSP',
            message : errSpExecution.message,
          });
        }
        parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
          const blackjackData = result.Games.BlackJack[0];
          const baccaratData = result.Games.Baccarat[0];
          const americanRouletteData = result.Games.Roulette_A[0];
          const europeanRouletteData = result.Games.Roulette_E[0];
          const sideBetPayoutData = result.Games.Side_Bet_Payout[0];
          const tableSideBetLimits = _.map(result.Games.TableSideBetLimits[0].Tables[0].Table, function tableData (table) {
            return {
              tableId   : table.TableId[0],
              UID       : table.UID[0],
              name      : table.Side_BetName[0].trim(),
              sideBetId : table.Side_BetId[0],
              sideMin   : table.TableMin[0],
              sideMax   : table.TableMax[0],
            };
          });

          if (error) {
            reject(error);
          }

          const lobbyData = {
            baccarat : {
              id     : baccaratData.$.Casino_Game_Id,
              active : baccaratData.$.Active,
              tables : _.map(baccaratData.Tables[0].Table, function tableData (table) {
                return {
                  id            : table.TableId[0],
                  UID           : table.UID[0],
                  minBet        : table.MINBET[0],
                  maxBet        : table.MAXBET[0],
                  dealerId      : table.DealerId[0],
                  dealerName    : table.NickName[0],
                  picture       : table.Picture[0],
                  sideBetLimits : _.filter(tableSideBetLimits, function sidebetLimitData (sideBetLimit) {
                    return sideBetLimit.UID === table.UID[0] &&
                      sideBetLimit.tableId === table.TableId[0];
                  }),
                };
              }),
            },
            americanRoulette : {
              id     : americanRouletteData.$.Casino_Game_Id,
              active : americanRouletteData.$.Active,
              tables : _.map(americanRouletteData.Tables[0].Table, function tableData (table) {
                return {
                  id         : table.TableId[0],
                  UID        : table.UID[0],
                  minBet     : table.MINBET[0],
                  maxBet     : table.MAXBET[0],
                  dealerId   : table.DealerId[0],
                  dealerName : table.NickName[0],
                  picture    : table.Picture[0],
                };
              }),
            },
            europeanRoulette : {
              id     : europeanRouletteData.$.Casino_Game_Id,
              active : europeanRouletteData.$.Active,
              tables : _.map(europeanRouletteData.Tables[0].Table, function tableData (table) {
                return {
                  id         : table.TableId[0],
                  UID        : table.UID[0],
                  minBet     : table.MINBET[0],
                  maxBet     : table.MAXBET[0],
                  dealerId   : table.DealerId[0],
                  dealerName : table.NickName[0],
                  picture    : table.Picture[0],
                };
              }),

            },
            blackjack : {
              id              : blackjackData.$.Casino_Game_Id,
              active          : blackjackData.$.Active,
              winPayout       : blackjackData.$.WinPayout,
              bjkPayout       : blackjackData.$.BJKPayout,
              insurancePayout : blackjackData.$.InsurancePayout,
              tables          : _.map(blackjackData.Tables[0].Table, function tableData (table) {
                return {
                  id            : table.TableId[0],
                  UID           : table.UID[0],
                  freeSpots     : (table.FREESPOTS) ? table.FREESPOTS[0].split(',') : [],
                  minBet        : table.MINBET[0],
                  maxBet        : table.MAXBET[0],
                  dealerId      : table.DealerId[0],
                  dealerName    : table.NickName[0],
                  picture       : table.Picture[0],
                  sideBetLimits : _.filter(tableSideBetLimits, function sidebetLimitData (sideBetLimit) {
                    return sideBetLimit.UID === table.UID[0] &&
                      sideBetLimit.tableId === table.TableId[0];
                  }),
                };
              }),
            },
            sideBetPayouts : _.map(sideBetPayoutData.Tables[0].Table, function payoutData (table) {
              return {
                sideBetName   : table.Side_BetName[0],
                sideBetPayout : table.Side_Bet_PayoutId[0],
                sideBetId     : table.Side_BetId[0],
                payout        : table.Payout[0],
                hand          : table.Hand[0],
              };
            }),
          };
          resolve(lobbyData);
        });
      });
    });
  });
}
