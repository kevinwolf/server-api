import sql from 'mssql';
import config from '../../config/environment';
import { encrypt } from '../../utils/crypto';
import {parseString} from 'xml2js';
import lobbyData from './lobbyData';
import async from 'async';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'LoginFailed', message : errSpConnection.message });
      }

      /* eslint-disable new-cap */
      request.input('Login', sql.VarChar(100), req.body.Login);
      request.input('Password', sql.VarChar(100), req.body.Password);
      request.input('Licensee_Id', sql.Int, req.body.Licensee);
      request.input('RemoteId', sql.Int, req.body.Remote);
      request.input('param_IS_DEBUG', sql.Bit, 0);
      request.output('Authenticated', sql.Bit);
      request.output('CustomerId', sql.Int);
      request.output('LoginStatus', sql.TinyInt);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spLogin_Customer]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'LoginFailed', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            async.parallel([
              function lobbyCallback (callback) {
                lobbyData().then(function processLobbyData (processedLobby) {
                  callback(null, processedLobby);
                }).catch(function catchErr (err) {
                  callback(err);
                });
              },
            ], function resolveParallel (err, results) {
              console.log('dataset', JSON.stringify(dataset));
              if (err) {
                res.status(400).json({
                  message : err.message,
                });
              } else {
                parseString(dataset[0][0][''][0], (error, result) => {
                  if (error) {
                    res.status(400).json({
                      message : error.message,
                    });
                  } else {
                    res.status(200).json({
                      balance    : parseFloat(result.CUSTOMER_LOGIN.BALANCE[0], 10),
                      token      : encrypt(`${encrypt('CUSTOMER')}|${encrypt(req.body.Login)}|${encrypt(req.body.Password)}`),
                      customerId : result.CUSTOMER_LOGIN.CUSTOMER_ID[0],
                      lobbyData  : results[0],
                    });
                  }
                });
              }
            });
          }else {
            res.status(401).json({
              error   : 'LoginFailed',
              message : request.parameters.output_STATUS.value,
            });
          }
        }
      });
    });
  } catch (err) {
    res.status(401).json({ error : 'LoginFailed', message : 'The Login information is incorrect' });
  }
}
