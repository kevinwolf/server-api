import sql from 'mssql';
import config from '../../config/environment';
import { encrypt } from '../../utils/crypto';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'CreateCustomerFailed', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('Login', sql.VarChar(100), 'alberto-cole');
      request.input('Password', sql.VarChar(100), encrypt('alberto-cole'));
      request.input('NickName', sql.VarChar(30), 'AppleFanboy');
      request.input('LanguageId', sql.Int, 1);
      request.input('CurrencyId', sql.Int, 1);
      request.input('WebSiteId', sql.Int, 1);
      request.input('AgentId', sql.Int, 1);
      request.input('RemoteId', sql.TinyInt, null);
      request.input('Chat_Status', sql.VarChar(25), null);
      request.input('ProfileId', sql.Int, 1);
      request.output('CustomerId', sql.Int);

      /* eslint-enable new-cap */
      request.execute('[dbo].[spAdd_Customer]', function spExecution (errSpExecution, dataset) {
        console.log(request.parameters);
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'CreateCustomerFailed', message : errSpExecution.message });
        } else {
          res.status(200).json(dataset);
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'CreateCustomerFailed', message : 'The Login information is incorrect' });
  }
}
