import sql from 'mssql';
import config from '../../config/environment';
import {parseString} from 'xml2js';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const formatCustomerPlayData = function format (xmlInfo) {
  return new Promise((resolve, reject) => {
    parseString(xmlInfo, function parseProcedure (error, result) {
      if (error) {
        reject(error);
      }
      resolve({
        balance  : result.Customer.Balance[0],
        nickname : result.Customer.NickName[0],
        language : result.Customer.Language[0],
        currency : result.Customer.Currency[0],
      });
    });
  });
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to get Customer Play Info', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      request.input('param_CUSTOMER_ID', sql.Int, req.params.customerId);
      request.input('param_CUSTOMER_TOKEN', sql.VarChar(250), '0');
      request.input('IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGetCustomerStartPlay]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to get Customer Play Info', message : errSpExecution.message });
        } else {
          // if (request.parameters.output_IS_SUCCESSFUL.value) {
          formatCustomerPlayData(dataset[0][0][''][0]).then((customerPlayData) => {
            res.status(200).json(customerPlayData);
          }).catch((err) => {
            res.status(401).json({
              error   : 'Oh Oh...',
              message : err.message,
            });
          });
          /* }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to get Customer Play Info',
            });
          } */
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingWebsite', message : 'Unable to create Website, try again' });
  }
}
