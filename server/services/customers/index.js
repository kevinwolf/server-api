import express from 'express';

const router = new express.Router();
router.post('/auth', require('./auth'));
router.post('/create', require('./create'));
router.post('/save-betbehind-settings', require('./save-betbehind-settings'));

router.get('/lobbyData', require('./lobbyDataHttp'));
router.get('/customerPlay/:customerId', require('./customerPlay'));

export default router;
