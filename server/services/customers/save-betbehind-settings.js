import sql from 'mssql';
import config from '../../config/environment';
// import {parseString} from 'xml2js';
import json2xml from 'json2xml';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'CreateSettingsFailed', message : errSpConnection.message });
      } else {
        const params = {
          CustomerId    : req.body.customerId,
          Casino_GameId : '1',
          Double        : req.body.double,
          Split         : req.body.split,
        };
        const settingsXml = json2xml({Decisions : {Detail : params, attr : params}}, { attributes_key : 'attr' });
        /* eslint-disable new-cap */
        request.input('BHDecisions_Param', sql.Xml, settingsXml);
        request.input('param_IS_DEBUG', sql.Bit, 0);
        request.output('output_IS_SUCCESSFUL', sql.Bit);
        request.output('output_STATUS', sql.VarChar(500));
        /* eslint-enable new-cap */
        request.execute('[dbo].[spAdd_BetBehind_Decisions]', function spExecution (errSpExecution) { // dataset
          connection.close();
          if (errSpExecution) {
            res.status(401).json({ error : 'CreateSettingsFailed', message : errSpExecution.message });
          } else {
            res.status(200).json({
              sucess : 'Settings were succesfully created',
            });
          }
        });
      }
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'CreateSettingsFailed', message : 'Unable to create settings' });
  }
}
