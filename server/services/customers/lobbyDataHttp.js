import lobbyData from './lobbyData';
export default function (req, res) {
  try {
    lobbyData().then(function processLobbyData (processedLobby) {
      res.status(200).json(processedLobby);
    }).catch(function catchErr (err) {
      res.status(401).json({ error : 'NoLobbyData', message : err.message });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NoLobbyData', message : err.message });
  }
}
