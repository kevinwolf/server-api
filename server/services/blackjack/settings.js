import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const xmlData = '<BJK_Settings><Detail Casino_GameId = "4" NumDecks = "100" DealerMin = "1" HitSoft17 = "12" Timer_DealerPeek = "9" Timer_Decisions = "7" Timer_Insurance = "28" Timer_EndGame = "1" Timer_PlaceBets = "1" Timer_KickOut = "4" WinPayout = "250" BJKPayout = "100" InsurancePayout = "100" DoublePayout = "100" BJKPayoutSuited = "100" BJKPayoutTied = "100" BJKAfterSplit = "100" DoubleDown = "100" DoubleAfterSplit = "100" DoubleForBalance = "100" DoubleAfterDouble = "100" MaxSplits = "100" SplittingAces = "100" ResplittingAces = "100" TiePayout17 = "100" TiePayout18 = "100" TiePayout19 = "100" TiePayout20 = "100" TiePayout21 = "100"></Detail></BJK_Settings>';

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to create setting', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('BJK_Settings_Param', sql.Xml, xmlData);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spBJK_AddSettings]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to create setting', message : errSpExecution.message });
        } else {
          console.log('request.parameters', request.parameters);
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              console.log('settings', error);
            }
            dataset[0][0][''][0] = result;
          });
          console.log('dataset', JSON.stringify(dataset));
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            res.status(200).json({
              success : 'New setting successfully created.',
            });
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to create setting',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingTable', message : 'Unable to create setting, try again' });
  }
}
