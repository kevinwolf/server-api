import express from 'express';

const router = new express.Router();

// GET METHODS
router.get('/get-lastbet/:customerId', require('./get-lastbet'));
router.get('/add-gamelog', require('./add-gamelog'));

router.post('/create-table', require('./create-table'));
router.post('/cust-actions', require('./cust-actions'));
router.post('/add-sidebet', require('./add-sidebet'));
router.post('/settings', require('./settings'));

export default router;
