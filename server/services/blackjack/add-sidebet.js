import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    console.log('si entro');
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to add sidebet', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      const params = {
        GameId      : req.body.gameId,
        CustomerId  : req.body.customerId,
        SideBetName : req.body.sidebetName,
        BetResult   : req.body.betResult,
        BetAmount   : req.body.betAmount,
      };
      const gameXml = json2xml({SideBet_Log : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
      request.input('SideBet_Log_Param', sql.Xml, gameXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spAdd_SideBet_Log]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to add sidebet', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            res.status(200).json({
              message : request.parameters.output_STATUS.value,
            });
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to add sidebet',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingTable', message : 'Unable to add sidebet, try again' });
  }
}
