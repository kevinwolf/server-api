import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const xmlData = '<TableGame><Detail BJK_TableId = "20" UID = "800.905.200" NumSpots = "1" TableMin = "10" TableMax = "400" MaxSplits = "2" SplitAces = "5" Double = "12" AllowSideBets = "1" Entity_SideBets = "1" Active = "1"></Detail></TableGame>';

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to create table', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('TableGame_Param', sql.Xml, xmlData);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spBJK_CreateTable]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to create table', message : errSpExecution.message });
        } else {
          console.log('request.parameters', request.parameters);
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              console.log('Create table', error);
            }
            dataset[0][0][''][0] = result;
          });
          console.log('dataset', JSON.stringify(dataset));
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            res.status(200).json({
              success : 'New table successfully created.',
            });
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to create table',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingTable', message : 'Unable to create table, try again' });
  }
}
