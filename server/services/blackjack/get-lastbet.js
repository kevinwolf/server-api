import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

let toJsonBet;
let betData;

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Oh oh! Unable to get last bet', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('param_CustomerId', sql.Int, req.params.customerId || req.body.customerId);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGet_SideBetLog]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to get last bet :(', message : errSpExecution.message });
        } else {
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              console.log('last-bet', error);
            }
            toJsonBet = dataset[0][0][''][0] = result;
          });
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            if (toJsonBet && toJsonBet.SideBetLog && toJsonBet.SideBetLog.Detail) {
              betData = toJsonBet.SideBetLog.Detail[0].Table;
              if (betData) {
                betData = betData.map(function dealerFn (dealer) {
                  Object.keys(dealer).forEach(function keyFn (key) {
                    dealer[key] = dealer[key][0];
                  });
                  return dealer;
                });
              }
              res.status(200).json({betData});
            }
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to get last bet',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NoLastBetSaved', message : 'Unable to get last bet, try again' });
  }
}
