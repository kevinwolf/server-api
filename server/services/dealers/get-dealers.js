import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

let toJsonDealers;
let dealersData;

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Oh oh! Unable to get dealers', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGetDealers]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to get dealers :(', message : errSpExecution.message });
        } else {
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              console.log('add-game', error);
            }
            toJsonDealers = dataset[0][0][''][0] = result;
          });

          if (request.parameters.output_IS_SUCCESSFUL.value) {
            if (toJsonDealers && toJsonDealers.Dealers && toJsonDealers.Dealers.Detail) {
              dealersData = toJsonDealers.Dealers.Detail[0].Table;
              dealersData = dealersData.map(function dealerFn (dealer) {
                Object.keys(dealer).forEach(function keyFn (key) {
                  dealer[key] = dealer[key][0];
                });
                return dealer;
              });
              console.log(dealersData);
              res.status(200).json({dealersData});
            }
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to get dealers',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NoListOfDealers', message : 'Unable to get dealers, try again' });
  }
}
