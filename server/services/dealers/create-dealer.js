import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
import getDealer from './get-dealers';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to create dealer', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      const dealerXml = json2xml({Dealers : {Detail : req.body, attr : req.body}}, { attributes_key : 'attr'});
      request.input('Dealer_Param', sql.VarChar(1000), dealerXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spAdd_Dealers]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to create dealer', message : errSpExecution.message });
        } else {
          console.log('request.parameters', request.parameters);
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            getDealer(req, res);
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to create dealer',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingDealer', message : 'Unable to create dealer, try again' });
  }
}
