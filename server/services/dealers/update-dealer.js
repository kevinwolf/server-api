import sql from 'mssql';
import {parseString} from 'xml2js';
import json2xml from 'json2xml';
import config from '../../config/environment';
import getDealer from './get-dealers';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to update dealer', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      const dealerXml = json2xml({Dealers : {Detail : {}, attr : req.body}}, { attributes_key : 'attr'});
      console.log(dealerXml);
      request.input('Dealer_Param', sql.VarChar(1000), dealerXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spUpdate_Dealers]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to update dealer', message : errSpExecution.message });
        } else {
          console.log('request.parameters', request.parameters);
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              console.log('add-game', error);
            }
            dataset[0][0][''][0] = result;
          });
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            console.log('ENTERING IN DELETE MODE');
            getDealer(req, res);
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to updated dealer',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotUpdatingDealer', message : 'Unable to update dealer, try again' });
  }
}
