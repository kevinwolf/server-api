import express from 'express';

const router = new express.Router();
// POST METHODS
router.post('/create-dealer', require('./create-dealer'));
router.post('/update-dealer', require('./update-dealer'));
router.post('/deactive-dealer', require('./deactive-dealer'));

// GET METHODS
router.get('/get-dealers', require('./get-dealers'));

export default router;
