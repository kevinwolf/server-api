import express from 'express';

const router = new express.Router();
router.post('/add-game', require('./add-game'));
router.post('/update-game', require('./update-game'));

export default router;
