import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const xmlData = '<CasinoGames><Detail CasinoGameId = "7" Name = "BlackJack" Description = "New Update" Active = "0"></Detail></CasinoGames>';

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to Update Casino Game', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('CasinoGames_Param', sql.Xml, xmlData);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spUpdate_CasinoGame]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to Update Casino Game', message : errSpExecution.message });
        } else {
          console.log('request.parameters', request.parameters);
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              console.log('add-game', error);
            }
            dataset[0][0][''][0] = result;
          });
          console.log('dataset', JSON.stringify(dataset));
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            res.status(200).json({
              success : 'Casino Game successfully Update.',
            });
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to Update Casino Game',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingTable', message : 'Unable to Update Casino Game, try again' });
  }
}
