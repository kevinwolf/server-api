import sql from 'mssql';
import json2xml from 'json2xml';
import config from '../../config/environment';
import listLicensees from './list';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Unable to create licensee', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      // We need to convert this JSON to XML in order to work right with the DB
      const params = {
        Name                   : req.body.name,
        Legal_Entity_Name      : req.body.legalEntityName,
        CurrencyId             : req.body.currency,
        LanguageId             : req.body.language,
        Additional_Languages   : req.body.additionalLanguage,
        Default_Max_Loss_Week  : req.body.defaultMaxLossWeek,
        Default_Max_Loss_Month : req.body.defaultMaxLossMonth,
        Default_Max_Win_Week   : req.body.defaultMaxWinWeek,
        Default_Max_Win_Month  : req.body.defaultMaxWinMonth,
        Royalti_Mini_Games     : req.body.royaltiMiniGames,
        Royalti_Tips           : req.body.royaltiTips,
        Royalti_All_Games      : req.body.royaltiAllGames,
        Monthly_Flat_Rate      : req.body.monthlyFlatRate,
        Invoice_Address        : req.body.invoiceAddress,
        Invoice_GroupBy        : req.body.invoiceGroupBy,
        Currency_Invoice       : req.body.currencyInvoice,
        Email                  : req.body.email,
        Telephone              : req.body.telephone,
        Tec_Email              : req.body.tecEmail,
        Tec_Telephone          : req.body.tecTelephone,
        CS_Email               : req.body.cSEmail,
        CS_Telephone           : req.body.cSTelephone,
        Account_Email          : req.body.accountEmail,
        Account_Telephone      : req.body.accountTelephone,
        EntityId               : req.body.entityId,
        Active                 : (req.body.active) ? '1' : '0',
      };
      const licenseeXml = json2xml({Licensee : {Detail : {}, attr : params}}, { attributes_key : 'attr' });
      console.log(licenseeXml);
      request.input('Licensee_Param', sql.VarChar(1000), licenseeXml);
      request.input('param_IS_DEBUG', sql.Bit, null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spAdd_Licensee]', function spExecution (errSpExecution) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to create licensee', message : errSpExecution.message });
        } else {
          if (request.parameters.output_IS_SUCCESSFUL.value) {
            listLicensees(req, res);
          }else {
            res.status(401).json({
              error   : 'Oops! Try again',
              message : 'Unable to create Licensee',
            });
          }
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NotCreatingLicensee', message : 'Unable to create Licensee, try again' });
  }
}
