import sql from 'mssql';
import {parseString} from 'xml2js';
import config from '../../config/environment';
import _ from 'lodash';

const spConfig = {
  user     : config.db_user,
  password : config.db_password,
  server   : config.db_host,
  port     : config.db_port,
  database : config.db_name,
};

const formatResult = function formatResult (result) {
  let formattedResult = {};
  try {
    if (result.Licensee.Detail[0].Table[0]) {
      formattedResult = _.map(result.Licensee.Detail[0].Table, function licenseeMap (lincenseeObj) {
        console.log(lincenseeObj);
        return {
          id               : lincenseeObj.Licensee_Id[0],
          name             : lincenseeObj.Name[0],
          legalName        : lincenseeObj.Legal_Entity_Name[0],
          currency         : lincenseeObj.CurrencyId[0],
          language         : lincenseeObj.LanguageId[0],
          maxLossWeek      : lincenseeObj.Default_Max_Loss_Week[0],
          maxLossMonth     : lincenseeObj.Default_Max_Loss_Month[0],
          currencyInvoice  : lincenseeObj.Currency_Invoice[0],
          cSEmail          : lincenseeObj.CS_Email[0],
          cSTelephone      : lincenseeObj.CS_Telephone[0],
          maxWinWeek       : lincenseeObj.Default_Max_Win_Week[0],
          maxWinMonth      : lincenseeObj.Default_Max_Win_Month[0],
          royaltiAllGames  : lincenseeObj.Royalti_All_Games[0],
          royaltiMiniGames : lincenseeObj.Royalti_Mini_Games[0],
          royaltiTips      : lincenseeObj.Royalti_Tips[0],
          monthlyFlatRate  : lincenseeObj.Monthly_Flat_Rate[0],
          invoiceAddress   : lincenseeObj.Invoice_Address[0],
          invoiceGroupBy   : lincenseeObj.Invoice_GroupBy[0],
          invoiceCurrency  : lincenseeObj.Currency_Invoice[0],
          email            : lincenseeObj.Email[0],
          telephone        : lincenseeObj.Telephone[0],
          tecEmail         : lincenseeObj.Tec_Email[0],
          tecTelephone     : lincenseeObj.Tec_Telephone[0],
          csEmail          : lincenseeObj.CS_Email[0],
          csTelephone      : lincenseeObj.CS_Telephone[0],
          accountEmail     : lincenseeObj.Account_Email[0],
          accountTelephone : lincenseeObj.Account_Telephone[0],
          entityId         : lincenseeObj.EntityId[0],
          active           : lincenseeObj.Active[0],
        };
      });
    }
  } catch (e) {
    console.log(e);
  }
  return formattedResult;
};
export default function (req, res) {
  try {
    const connection = new sql.Connection(spConfig, function spConnection (errSpConnection) {
      const request = connection.request();
      if (errSpConnection) {
        res.status(401).json({ error : 'Oh oh! Unable to get Licensees', message : errSpConnection.message });
      }
      /* eslint-disable new-cap */
      request.input('LicenseeId', null);
      request.output('output_IS_SUCCESSFUL', sql.Bit);
      request.output('output_STATUS', sql.VarChar(500));
      /* eslint-enable new-cap */
      request.execute('[dbo].[spGetLicensee]', function spExecution (errSpExecution, dataset) {
        connection.close();
        if (errSpExecution) {
          res.status(401).json({ error : 'Unable to get Licensees :(', message : errSpExecution.message });
        } else {
          // dataset[0][0][''][0] this is made in order to get the position of the
          // property that we need to convert into JSON
          parseString(dataset[0][0][''][0], function parseProcedure (error, result) {
            if (error) {
              res.status(401).json({ error : 'Incorrect Parse', message : error.message });
            }
            if (request.parameters.output_IS_SUCCESSFUL.value) {
              res.status(200).json(formatResult(result));
            }else {
              res.status(401).json({
                error   : 'Output not successful',
                message : request.parameters.output_STATUS,
              });
            }
          });
        }
      });
    });
  } catch (err) {
    console.log(err);
    res.status(401).json({ error : 'NoListOfDealers', message : 'Unable to get dealers, try again' });
  }
}
