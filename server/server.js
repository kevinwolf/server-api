import express from 'express';
import chalk from 'chalk';
import config from './config/environment';

const app = express();

require('./config/express')(app);
require('./config/routes')(app);

app.listen(config.port, () => {
  console.log(chalk.white(`\nAPI server listening on port ${chalk.bold(config.port)} in ${chalk.bold(app.get('env'))} mode.\n`));
});

export default app;
